from twilio.rest import Client
from flask import Flask, request, redirect
app = Flask(__name__)
from twilio.twiml.messaging_response import MessagingResponse
# Your Account SID from twilio.com/console
account_sid = "AC621cf6521916d77e58bcba3c3c6785fb"
# Your Auth Token from twilio.com/console
auth_token  = "f2e4b835b894c65052629a49a9f8db42"

dictionary = {
    "LOL": "laugh out loud",
    "PBX": "Private branch exchange",
    "PSTN": "Public switch telephone network",
    "PDD": "Post dial delay",
    "DID": "Direct inward dialing (phone number)",
    "URI": "Uniform Resource Identifier",
    "SIP": "Session initation protocol",
    "RTC": "Real time communication",
    "B2B UA": "Back to back user agent",
    "PC": "Proxy core",
    "PNI": "Private network indication",
    "VPN": "Virtual Private Network",
    "CNAM": "Caller Name",
    "CDR": "Call Detail Record",
    "ISV": "Independent Software Vendor",
    "PRI": "Primary Rate Interface (landline)",
    "VOIP": "Voice Over Internet Protocol",
    "CPS": "Calls per second",
    "SID": "Security identifier AKA String ID",
    "SDK": "Software Development Kit",
    "IVR": "Interactive Voice Response",
    "MPLS": "Multiprotocol label switching",
    "SRTP": "Secure Real-time Transport Protocol",
    "TLS": "Transport Layer Security",
    "E911": "Enhanced 9-1-1",
    "HELLO": "Hi there! What can I unabbrev for you?",
    "HI": "Hi there! What can I unabbrev for you?",
    "BQR": "Best quality route",
    "LCR": "Least cost route",
    "IVR": "Interactive voice request",
    "SLA": "Service Level Agreement",
    "SMS": "Short messaging service",
    "ULC": "Underlying carrier",
    "FU": "F*** you",
    "F U": "F*** you",
    "OMG": "Objectives & measurable goals",
    "TLS": "Transport Layer Security",
    "SBC": "Session Border Control",
    "DLR": "Delivery receipts",
    "ACD": "Average Call Duration",
    "NPA": "Area Code",
    "TFN": "Toll Free Number",
    "LNP": "Local Number Porting",
    "THANKS!": "No problem Twilion",
    "THANKS": "No problem Twilion",
    "THANK YOU": "No problem Twilion",  

}

@app.route("/",methods=["POST"])
def respond():
    body=request.values.get("Body").upper()
    from_number=request.values.get("From")
    if body in dictionary:
        message=dictionary[body]
    else:
        message="sorry, I don't know that one"
    resp = MessagingResponse()
    resp.message(message)
    return str(resp)

def send_message(to_number,message):
    client = Client(account_sid, auth_token)
    message = client.messages.create(
        to=to_number, 
        from_="+18474236879",
        body=message)

if __name__ == "__main__":
    app.run(debug=True)
